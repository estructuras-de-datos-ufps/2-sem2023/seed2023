/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Vista;

import Util.seed.ArbolB;
import Util.seed.NodoB;
import Util.seed.BTreePrinter;
import java.util.Iterator;

/**
 *
 * @author Docente
 */
public class TestArbolGeneral {
    
    public static void main(String[] args) {
        ArbolB<Integer> t = new ArbolB();
        
        NodoB<Integer> n1, n2, n3, n4, n5;
        //Hojas:
        n5 = new NodoB(6);
        n4 = new NodoB(5);
        n2 = new NodoB(3);
        //Armar el árbol:
        n3 = new NodoB(2, n4, n5);
        n1 = new NodoB(9, n3, n2);
        t.setRaiz(n1);
        
        
        System.out.println("Preorden:");
        imprimir(t.preOrden());
        
        System.out.println("\nInorden:");
        imprimir(t.inOrden());
        
        System.out.println("\nPosorden:");
        imprimir(t.posOrden());
        
        System.out.println("\nPre derechos:");
        imprimir(t.preOrdenDerechos());
        
        System.out.println("\nHojas:");
        imprimir(t.getHojas());
        
        System.out.println("\nCardinalidad");
        System.out.println(t.getCardinalidad());
        
        System.out.println("\nCardinalidad Computer Science:");
        System.out.println(t.getCardinalidad2());
        
        System.out.println("\nCardinalidad nodos externos:");
        System.out.println(t.getCardinalidadExternos());
        
        System.out.println("\nNodo menor:");
        System.out.println(t.getMenor());
        
        
        System.out.println("Preorden Iterativo:");
        imprimir(t.getPreOrdenIterativo());
        
        
        System.out.println("\nRecorrido por anchura:");
        imprimir(t.getRecorridoAnchura());
        
        System.out.println("\nimprimiendo Nivel 2:");
        imprimir(t.getNiveles(2));
        
        
        BTreePrinter.printNode(t.getRaiz());
    }
    
    
    private static <T> void imprimir(Iterator<T> it)
    {
        
        while(it.hasNext())
            System.out.print(it.next().toString()+"\t");
        System.out.println();
    }
    
}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Vista;

import Modelo.Estudiante;
import Util.seed.*;
import java.util.Iterator;

/**
 *
 * @author docente
 */
public class TestBinarioBusquedaEstudiante {
    public static void main(String[] args) {
        Estudiante x=new Estudiante(1, "madarme",0F,0F,0F,0F);
        Estudiante y=new Estudiante(5, "madarme",0F,0F,0F,0F);
        Estudiante z=new Estudiante(3, "madarme",0F,0F,0F,0F);
        Estudiante w=new Estudiante(2, "madarme",0F,0F,0F,0F);
        
        ArbolBinarioBusqueda<Estudiante> t=new ArbolBinarioBusqueda();
        t.insertar(w);
        t.insertar(z);
        t.insertar(y);
        t.insertar(x);
        
        //t.imprime();
        imprimir(t.inOrden());
        //BTreePrinter.printNode(t.getRaiz());
        
        
    }
    
    
    private static <T> void imprimir(Iterator<T> it)
    {
        
        while(it.hasNext())
            System.out.print(it.next().toString()+"\t");
        System.out.println();
    }
    
}

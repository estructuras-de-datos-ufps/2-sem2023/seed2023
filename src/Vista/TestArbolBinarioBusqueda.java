/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Vista;

import Util.seed.*;

/**
 *
 * @author madar
 */
public class TestArbolBinarioBusqueda {
    
    public static void main(String[] args) {
        ArbolBinarioBusqueda<Integer> t=new ArbolBinarioBusqueda();
        t.insertar(4);
        t.insertar(5);
        t.insertar(6);
        t.insertar(7);
        t.insertar(1);
        t.insertar(2);
        BTreePrinter.printNode(t.getRaiz());
    }
}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Vista;

/**
 * Programa imprime si una cadena es el inverso de su subcadena
 * cuando son separadas por &
 * @author DOCENTE
 */
public class TestInfijo {
    
    public static void main(String[] args) {
        String cadena="UFPS&SPFU"; //--> true
        /**
         * LO QUE ESTÁ ANTES DEL & SE COLOCA EN UNA COLA
         * LO QUE ESTÁ DESPUÉS EN UNA PILA
         * Y SE COMPARA LA PILA Y LA COLA
         */
        
    }
    
}
